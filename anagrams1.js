const button = document.getElementById("findButton");

//atribuir uma função de handler de clique que será executada quando o botão clicado
button.onclick = function getAnagramsOf() {
    
    document.getElementById("output").innerText = "";

    //acessar o conteúdo digitado na caixa de texto
    let typedText = document.getElementById("input").value;
    
    //ordenar as letras alfabeticamente para detectar anagramas
    function alphabetize(a) {
        return a.toLowerCase().split("").sort().join("").trim();
    }
    
    //retornar um novo array com matches em palavras.js
    let matchInPalavras = [];
    for (let i = 0; i < palavras.length; i++) {

        if (alphabetize(typedText) === alphabetize(palavras[i])) {
            matchInPalavras.push(palavras[i]);
        }
    }
    console.log(matchInPalavras)

    //display output
    for (let anagrams of matchInPalavras) {
        let output = document.createElement("span");
        let textContent = document.createTextNode(anagrams + " ");    
        output.appendChild(textContent);
        document.getElementById("output").appendChild(textContent);
    }
    console.log(document.getElementById("output"))
}